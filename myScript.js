$(document).ready(function() {
    var baseHtml = "http://tinman.cs.gsu.edu/~raj/2010/sp10/project/";
    var imageList = ["p1.gif","p2.gif","p3.gif","p4.gif","p5.gif","p6.gif","p7.gif","p8.gif"];
    var start = 0;

    var board;
    var hidden;
    var clickNum;
    var clickedIndex;
    var numOpen;

  $("#ready").click(function() {
    $("#timer").html("");

    //construct the initial board
    board = ["","","","","","","","","","","","","","","",""];
    hidden = [true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true];
    clickNum = 0;
    clickedIndex = [0,1];
    numOpen = 0;

    for(var i = 0; i < 8; i++){
        var pos = Math.floor(Math.random()*16);
        while(board[pos] != "") {
            pos = Math.floor(Math.random() * 16);
        }
        board[pos] = imageList[i];

        pos = Math.floor(Math.random()*16);
        while(board[pos] != "")
            pos = Math.floor(Math.random()*16);
        board[pos] = imageList[i];
    }

    //add to table
    for(var j = 0; j < 16; j++) {
        var htmlString = "<img src=\"" + baseHtml + "openImages/" + board[j] + "\">"
        $("#"+j).html(htmlString);
    }

    //disable clicks
    //$('#board').off('click'); // click is succesfully removed

    //wait for 3 seconds and display numbers
    setTimeout(function(){
        for(var j = 0; j < 16; j++) {
            var htmlString = "<img src=\"" + baseHtml + "hideImages/" + (j+1) + ".gif\">"
            $("#"+j).html(htmlString);
        }
    }, 3000);

    start = new Date().getTime();

  });//end of ready button click


    //enable clicks

    $("#board td").on('click', function () {
        var col = parseInt( $(this).index() ) ;
        var row = parseInt( $(this).parent().index() );
        var index = 4*row + col;

        //if image hasnt already been clicked
        if(clickNum == 0) {
            if (hidden[index]) {
                var htmlString = "<img src=\"" + baseHtml + "openImages/" + board[index] + "\">"
                $("#"+index).html(htmlString);
                hidden[index] = false;
                clickNum = 1;
                clickedIndex[0]= index;
            }
        }
        else{
            if (hidden[index]) {
                var htmlString = "<img src=\"" + baseHtml + "openImages/" + board[index] + "\">"
                $("#"+index).html(htmlString);
                hidden[index] = false;
                clickNum = 0;
                clickedIndex[1]= index;
                if(board[clickedIndex[0]] != board[clickedIndex[1]]){
		    $.playSound("crash");
                    //$('#board').off('click'); // click is succesfully removed
                    setTimeout(function(){
                        var htmlString = "<img src=\"" + baseHtml + "hideImages/" + (clickedIndex[0]+1) + ".gif\">"
                        $("#"+clickedIndex[0]).html(htmlString);
                        htmlString = "<img src=\"" + baseHtml + "hideImages/" + (clickedIndex[1]+1) + ".gif\">"
                        $("#"+clickedIndex[1]).html(htmlString);
                        hidden[clickedIndex[0]] = true;
                        hidden[clickedIndex[1]] = true;
                    }, 1000);

                }
                else{
                    numOpen++;
                    if(numOpen == 8){
		       $.playSound("win");
                       var end = new Date().getTime();
		       var time = (end-start)/1000;	
		       $("#timer").html("<p>Congrats it took you: "+time+" seconds.</p>");
                        for(var j = 0; j < 16; j++) {
                            var htmlString = "<img src=\" http://tinman.cs.gsu.edu/~raj/2010/sp10/project/win.gif\">";
                            $("#"+j).html(htmlString);
                        }
                    }
		    else
		      $.playSound("yay");
                }
            }
        }
    });

});
